use array2d::Array2D;
use std::io::BufRead;
use std::fmt::{Display};
use std::fmt;

struct PrintWrap<T:Clone>(Array2D<T>);
impl<T: Clone + Display> Display for PrintWrap<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in self.0.columns_iter(){
            for elem in row {
                write!(f, "{} ", elem)?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone)]
struct Point(usize, usize);

impl Point {
    fn parse_from_str(s: &str) -> Self {
        let parts = s.split(',').collect::<Vec<_>>();
        Point(parts[0].parse().unwrap(), parts[1].parse().unwrap())
    }
}

#[derive(Debug, Clone)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn parse_from_line(s: &str) -> Self {
        let parts = s.split(" -> ").collect::<Vec<_>>();
        let start = Point::parse_from_str(&parts[0]);
        let end = Point::parse_from_str(&parts[1]);
        Line { start, end }
    }

    /// IF this is a horizontal line, returns the fixed y-coordinate
    fn horiz_coord(&self) -> Option<usize> {
        if self.start.1 == self.end.1 {
            return Some(self.start.1);
        }
        None
    }

    /// IF this is a vertical line, returns the fixed x-coordinate
    fn vert_coord(&self) -> Option<usize> {
        if self.start.0 == self.end.0 {
            return Some(self.end.0);
        }
        None
    }

    fn covers(&self, pt: &Point) -> bool {
        if let Some(y) = self.horiz_coord() {
            let b = std::cmp::min(self.start.0, self.end.0);
            let e = std::cmp::max(self.start.0, self.end.0);
            if (b..=e).contains(&pt.0) && pt.1 == y {
                return true;
            }
            return false;
        } else if let Some(x) = self.vert_coord() {
            let b = std::cmp::min(self.start.1, self.end.1);
            let e = std::cmp::max(self.start.1, self.end.1);
            if (b..=e).contains(&pt.1) && pt.0 == x {
                return true;
            }
            return false;
        } else {
            panic!("{:?} is not horiz or vert!", self);
        }
    }
}

fn compute_maxcoord(lines: &[Line]) -> usize {
    let res = lines
        .iter()
        .map(|x| vec![x.start.0, x.start.1, x.end.0, x.end.1])
        .flatten()
        .collect::<Vec<_>>();
    let hi = res.iter().max().unwrap();
    *hi
}

fn main() {
    let file = std::fs::File::open("input.txt").unwrap();
    let lines = std::io::BufReader::new(file)
        .lines()
        .map(|x| Line::parse_from_line(&x.unwrap()))
        .collect::<Vec<_>>();

    let lines = lines
        .iter()
        .filter(|x| x.vert_coord().is_some() || x.horiz_coord().is_some())
        .cloned()
        .collect::<Vec<_>>();

    let max = compute_maxcoord(&lines[..]) + 1;
    let mut linects = Array2D::filled_with(0usize, max, max);

    for i in 0..max {
        for j in 0..max {
            let p = Point(i, j);
            for l in lines.iter() {
                if l.covers(&p) {
                    let z = linects.get_mut(i, j);
                    *(z.unwrap()) += 1;
                }
            }
        }
    }

    let nums = linects
        .elements_row_major_iter()
        .filter(|x| **x > 1)
        .collect::<Vec<_>>()
        .len();
    println!("{}", nums);
}
