use array2d::Array2D;
use std::io::BufRead;
use std::fmt::{Display};
use std::fmt;

struct PrintWrap<T:Clone>(Array2D<T>);
impl<T: Clone + Display> Display for PrintWrap<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in self.0.columns_iter(){
            for elem in row {
                write!(f, "{} ", elem)?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Point(usize, usize);

impl Point {
    fn parse_from_str(s: &str) -> Self {
        let parts = s.split(',').collect::<Vec<_>>();
        Point(parts[0].parse().unwrap(), parts[1].parse().unwrap())
    }
}

fn sgn(x: i32) -> i32 {
    if x == 0{
        return 0
    } else if x > 0 {
        return 1
    } else {
        return -1
    }
}

#[derive(Debug, Clone)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn parse_from_line(s: &str) -> Self {
        let parts = s.split(" -> ").collect::<Vec<_>>();
        let start = Point::parse_from_str(&parts[0]);
        let end = Point::parse_from_str(&parts[1]);
        Line { start, end }
    }


    fn covers(&self, pt: &Point) -> bool {
        let udstep = sgn(i32::try_from(self.end.1).unwrap() - i32::try_from(self.start.1).unwrap());
        let lrstep = sgn(i32::try_from(self.end.0).unwrap() - i32::try_from(self.start.0).unwrap());
        let Point(mut x, mut y) = self.start;

        loop {
            if Point(x, y) == self.end {
                return &Point(x,y) == pt;
            }
            if pt == &Point(x,y){
                return true;
            }
            x = usize::try_from(i32::try_from(x).unwrap() + lrstep).unwrap();
            y = usize::try_from(i32::try_from(y).unwrap() + udstep).unwrap();
        }
    }
}

fn compute_maxcoord(lines: &[Line]) -> usize {
    let res = lines
        .iter()
        .map(|x| vec![x.start.0, x.start.1, x.end.0, x.end.1])
        .flatten()
        .collect::<Vec<_>>();
    let hi = res.iter().max().unwrap();
    *hi
}

fn main() {
    let file = std::fs::File::open("input.txt").unwrap();
    let lines = std::io::BufReader::new(file)
        .lines()
        .map(|x| Line::parse_from_line(&x.unwrap()))
        .collect::<Vec<_>>();

    /*
    let lines = lines
        .iter()
        .filter(|x| x.vert_coord().is_some() || x.horiz_coord().is_some())
        .cloned()
        .collect::<Vec<_>>();
    */

    let max = compute_maxcoord(&lines[..]) + 1;
    let mut linects = Array2D::filled_with(0usize, max, max);

    for i in 0..max {
        for j in 0..max {
            let p = Point(i, j);
            for l in lines.iter() {
                if l.covers(&p) {
                    let z = linects.get_mut(i, j);
                    *(z.unwrap()) += 1;
                }
            }
        }
    }

    let nums = linects
        .elements_row_major_iter()
        .filter(|x| **x > 1)
        .collect::<Vec<_>>()
        .len();
    println!("{}", nums);
}
