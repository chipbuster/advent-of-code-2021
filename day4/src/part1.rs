use std::io::{BufRead, Read};
use array2d::Array2D;

const BOARD_SZ: usize = 5;

#[derive(Debug, Eq, PartialEq)]
struct Board {
    elems: Array2D<i8>,
    marked: Array2D<bool>,
}

impl Board {
    fn new_from_rows(s: &[i8]) -> Self {
        let elems = Array2D::from_iter_row_major(s.iter().cloned(), BOARD_SZ, BOARD_SZ);
        let marked = Array2D::filled_with(false, BOARD_SZ, BOARD_SZ);
        Board { elems, marked }
    }

    fn mark_location(&mut self, i: usize, j: usize) {
        self.marked.set(i,j,true).unwrap()
    }

    fn mark_number(&mut self, t: i8) {
        for i in 0..BOARD_SZ {
            for j in 0..BOARD_SZ {
                if *self.elems.get(i,j).unwrap() == t {
                    println!("Board element = {}", t);
                    self.mark_location(i, j)
                }
            }
        }
    }

    fn is_winning_board(&self) -> bool {
        for mut col in self.marked.columns_iter() {
            if col.all(|x| *x) {
                return true;
            }
        }

        for mut row in self.marked.rows_iter() {
            if row.all(|x| *x) {
                return true;
            }
        }

        false
    }

    fn board_score(&self, final_number: i8) -> i32 {
        println!("Winning board: {:?}", self);
        let mut score = 0i32;
        for i in 0..BOARD_SZ {
            for j in 0..BOARD_SZ {
                if !self.marked.get(i,j).unwrap() {
                    score += i32::from(*self.elems.get(i,j).unwrap());
                }
            }
        }
        println!("{}", score);
        score * i32::from(final_number)
    }
}

fn main() {
    let boardfile = std::fs::File::open("boards.txt").unwrap();
    let mut numfile = std::fs::File::open("numbers.txt").unwrap();

    let mut boards = read_boards(boardfile);
    let mut move_buf = Vec::new();
    let moves = numfile.read_to_end(&mut move_buf).unwrap();
    let moves = std::str::from_utf8(&move_buf[..]).unwrap().split(',').map(|x| x.parse::<i8>().unwrap()).collect::<Vec<_>>();

    'game: for n in moves {

        for b in boards.iter_mut() {
            b.mark_number(n);

            if b.is_winning_board(){
                println!("{}", b.board_score(n));
                break 'game;  // GAME OVER
            }
        }
        
        if n == 24{
            println!("{:?}", boards[2]);
        }
    }

}

fn read_boards(file: std::fs::File) -> Vec<Board> {
    let freader = std::io::BufReader::new(file).lines();
    let mut agg = String::new();
    let mut boards  = Vec::new();
    for line in freader {
        let line = line.unwrap();
        if line.is_empty(){
            let board = read_board(&agg);
            boards.push(board);
            agg.clear();
        } else {
            agg.push_str(&line);
            agg.push_str(" ");
        }
    }
    let board = read_board(&agg);
    boards.push(board);
    boards
}

fn read_board(s: &str) -> Board {
    let nums: Vec<i8> = s.split_ascii_whitespace().map(|x| x.parse::<i8>().unwrap()).collect();
    Board::new_from_rows(&nums[..])
}