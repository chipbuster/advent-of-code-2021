use std::io::BufRead;

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let fname = &args[1][..];
    let file = std::fs::File::open(fname).unwrap();
    let freader = std::io::BufReader::new(file).lines();

    let depths = freader
        .into_iter()
        .map(|x| x.unwrap().parse::<i32>().unwrap())
        .collect::<Vec<_>>();


    let mut inc_count = 0;
    for slice in depths.windows(2){
        if slice[1] > slice[0]{
            inc_count += 1;
        }
    }
    println!("{}", inc_count);
}
