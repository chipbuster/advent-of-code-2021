var fs = require("fs");  // file system        


function getFishList(counter: number, days: number): number[] {
  let fishes = [counter];
  for (let day = 0; day < days; day += 1) {
    let numNewFish = 0;
    for (let i = 0; i < fishes.length; i += 1) {
      if (fishes[i] === 0) {
        fishes[i] = 6;
        numNewFish += 1;
      } else {
        fishes[i] -= 1;
      }
    }
    for (let i = 0; i < numNewFish; i += 1) {
      fishes.push(8);
    }
  }
  return fishes
}


function numFish(counter: number, days: number): number {
  return getFishList(counter, days).length
}

function numTotalFish(fishes: number[], days: number): number {
  return fishes.map((ctr) => numFish(ctr, days)).reduce((a, b) => a + b, 0)
}

// For each starting counter, 
function fishCountMap(days: number): number[] {
  let output: number[] = new Array(9);
  for (let i = 0; i <= 8; i += 1) {
    output[i] = numFish(i, days);
  }
  return output;
}

function main() {
  var input = JSON.parse(fs.readFileSync('input.txt', 'utf8'))
  const fishes = input as number[];
  const fishmap128 = fishCountMap(128);
  let dictFishCt = fishes.map((ctr: number) => fishmap128[ctr]).reduce((a: number, b: number) => a + b, 0);
  console.log(`From dict: ${dictFishCt}`)
  console.log(`From explicit: ${numTotalFish(fishes, 128)}`);

  let num_fish_at_256: BigInt[] = new Array(7);
  for (let init_ctr = 0; init_ctr <= 6; init_ctr += 1) {
    let fish_list_for_ctr = getFishList(init_ctr, 128);

    let fish_256 = BigInt(0);
    fish_list_for_ctr.forEach((x) => {
      if (fishmap128[x] == null) {
        console.log(`Undefined fishmap for starting ${x}`)
      }
      fish_256 += BigInt(fishmap128[x]);
    })

    num_fish_at_256[init_ctr] = fish_256;
  }

  let total_fishes: BigInt = BigInt(0n);
  for (let i = 0; i < fishes.length; i += 1) {
    let val: BigInt = num_fish_at_256[fishes[i]];
    total_fishes = total_fishes.valueOf() + val.valueOf()
  }

  console.log(`256: ${total_fishes}`);
}

main();