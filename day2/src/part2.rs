use std::io::{BufRead};

#[derive(Debug, Eq, PartialEq)]
enum MoveCmd {
    Up(i32),
    Fwd(i32),
    Down(i32)
}

#[derive(Debug, Eq, PartialEq)]
struct Position {
    depth: i32,
    horiz: i32,
    aim: i32,
}

impl Position {
    fn apply_move(&mut self, mv: &MoveCmd){
        match mv {
            &MoveCmd::Up(x) => self.aim -= x,
            &MoveCmd::Down(x) => self.aim += x,
            &MoveCmd::Fwd(x) => {
                self.horiz += x;
                self.depth += self.aim * x;
            },
        }
    }
}

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let fname = &args[1][..];
    let file = std::fs::File::open(fname).unwrap();
    let freader = std::io::BufReader::new(file).lines(); 

    let mut moves = Vec::new();
    for line in freader{        
        let line = line.unwrap();
        let parts: Vec<&str> = line.split_ascii_whitespace().collect();
        match parts[0] {
            "forward" => moves.push(MoveCmd::Fwd(parts[1].parse().unwrap())),
            "up" => moves.push(MoveCmd::Up(parts[1].parse().unwrap())),
            "down" => moves.push(MoveCmd::Down(parts[1].parse().unwrap())),
            _ => panic!("Invalid movement specifier {}", parts[0]),
        }
    }

    let mut cur_pos = Position {depth: 0, horiz: 0, aim: 0};
    for m in moves {
        cur_pos.apply_move(&m);
    }

    println!("{}", cur_pos.depth * cur_pos.horiz);
}