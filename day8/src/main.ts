import { isContext } from "vm";

var fs = require("fs");  // file system
var lineReader = require("line-reader");

//* The number of characters the strings share in common */
function n_isct(x: string, y: string): number {
  let count = 0;
  let xa = [...x];
  let ya = [...y];
  xa.forEach(xc => {
    if (ya.includes(xc)) {
      count += 1;
    }
  })
  return count;
}

class DisplayData {
  signalPatterns: string[];
  displayed: string[];

  constructor(s: string) {
    let seen_sep = false;
    let signalPatterns = new Array();
    let displayed = new Array();

    s.split(" ").forEach(x => {
      // Sort the string
      let letters = [...x].sort();
      let sortedx = letters.join("");

      if (x == '|') {
        seen_sep = true;
      }
      else if (seen_sep) {
        displayed.push(sortedx);
      } else {
        signalPatterns.push(sortedx);
      }
    })
    this.signalPatterns = signalPatterns;
    this.displayed = displayed;
  }

  num_unique_digits(): number {
    let count = 0;
    this.displayed.forEach((x) => {
      if (x.length == 2 || x.length == 4 || x.length == 3 || x.length == 7) {
        count += 1;
      }
    })
    return count;
  }

  get_digit_mapping(): Map<string, number> {
    let dmap = new Map();

    this.signalPatterns.forEach(x => {
      if (x.length == 2) {
        dmap.set(1, x)
      } else if (x.length == 4) {
        dmap.set(4, x)
      } else if (x.length == 3) {
        dmap.set(7, x)
      } else if (x.length == 7) {
        dmap.set(8, x)
      }
    });


    let seg_6 = this.signalPatterns.filter(x => x.length === 6);
    let seg_5 = this.signalPatterns.filter(x => x.length === 5);

    // Solve 5-segment signals
    seg_5.forEach(x => {
      if (n_isct(dmap.get(1), x) == 2) {
        dmap.set(3, x);
      } else if (n_isct(dmap.get(4), x) == 3) {
        dmap.set(5, x);
      } else if (n_isct(dmap.get(4), x) == 2) {
        dmap.set(2, x);
      } else {
        console.log("ERROR ON SEG5");
      }
    })

    // Solve 6-segment signals
    seg_6.forEach(x => {
      if (n_isct(dmap.get(1), x) == 1) {
        dmap.set(6, x);
      } else if (n_isct(dmap.get(4), x) == 4) {
        dmap.set(9, x);
      } else if (n_isct(dmap.get(4), x) == 3) {
        dmap.set(0, x);
      } else {
        console.log("ERROR ON SEG6");
      }
    })

    console.assert(dmap.size === 10, "Map is badly sized")

    let revMap = new Map();
    dmap.forEach((v, k) => revMap.set(v, k));
    return revMap;
  }

  get_displayed(): number {
    let map = this.get_digit_mapping();
    let n = 0;
    this.displayed.forEach(x => {
      n *= 10;
      n += map.get(x)!;
    })
    return n;
  }

}


function main() {
  let input_strings: string[] = fs.readFileSync("input.txt", "utf8").toString().split('\n');
  let input = input_strings.map((x: string) => new DisplayData(x));

  let displayed = new Array();
  console.log(displayed);
  input.forEach(x => displayed.push(x.get_displayed()));
  let total = displayed.reduce((a, b) => a + b, 0);
  console.log(`${total}`)
}

main();