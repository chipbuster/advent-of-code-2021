var fs = require("fs");  // file system        

const illegalScore = new Map<string, number>([
  [")", 3],
  ["]", 57],
  ["}", 1197],
  [">", 25137],
]);

const incompleteScore = new Map<string, number>([
  ["(", 1],
  ["[", 2],
  ["{", 3],
  ["<", 4],
]); 

const closeToOpen = new Map<string, string>([
  [")", "("],
  ["]", "["],
  ["}", "{"],
  [">", "<"],
]);

const openToClose = new Map<string,string>();
closeToOpen.forEach((value, key) => openToClose.set(value, key));

function corruptionPoints(s: string): number {
  let stack = new Array<String>();
  for (let char of [...s]) {
    let open = closeToOpen.get(char);
    if(open == null){
      stack.push(char);
    } else {
      let last_char = stack.pop();
      if (last_char !== open){
        return illegalScore.get(char)!;
      }
    }
  }
  return 0;
}

function incompletePoints(s: string): number {
  let stack = new Array<string>();
  let completion = new Array<string>();

  for (let char of [...s]) {
    let open = closeToOpen.get(char);
    if(open == null){
      stack.push(char);
    } else {
      stack.pop();
    }
  }

  console.log(stack);
  let score = 0;
  while (stack.length > 0){
    score *= 5;
    score += incompleteScore.get(stack.pop()!)!;
  }
  return score;
}

function main() {
  var input: string[] = fs.readFileSync('input.txt', 'utf8').toString().split('\n');
  let totalScore = input.map(x => corruptionPoints(x)).reduce((a,b)=>a+b,0);
  console.log(totalScore);

  // All uncorrupted lines are incomplete.
  let incomplete = input.filter(x => corruptionPoints(x) === 0);
  let allIScores = incomplete.map(x => incompletePoints(x)).sort((a,b) => {return a-b;});
  let half = Math.floor(allIScores.length / 2);
  console.log(allIScores[half]);
}

main();
