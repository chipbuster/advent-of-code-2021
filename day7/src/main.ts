var fs = require("fs");  // file system        

function median(arr: number[]): number {
  const midpt = (arr.length - arr.length % 2) / 2;
  return arr[midpt];
}

function mean(arr: number[]): number {
  return arr.reduce((a, b) => a + b, 0) / arr.length;
}

function fuel_cost_1(x_pos: number[], position: number): number {
  let totalCost = 0;
  x_pos.forEach((x) => {
    totalCost += Math.abs(x - position)
  })
  return totalCost;
}

function fuel_cost_2(x_pos: number[], position: number): number {
  let totalCost = 0;
  x_pos.forEach((x) => {
    let ldist = Math.abs(x - position)
    let lcost = (ldist * (ldist + 1)) / 2;
    totalCost += lcost;
  })
  return totalCost;
}

function main() {
  console.log("GENERAL KENOBI.")
  var input = JSON.parse(fs.readFileSync('input.txt', 'utf8')) as number[];
  input.sort((a, b) => { return a - b; });
  // var input = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14];
  // input.sort((a, b) => { return a - b; });

  let m1 = median(input);
  let m2 = mean(input);

  console.log(m2);

  let mincost = 999999999;
  let minpos = 0;
  for (let trial = m1; trial <= Math.ceil(m2); trial += 1) {
    let trialcost = fuel_cost_2(input, trial);
    if (trialcost < mincost) {
      mincost = trialcost;
      minpos = trial;
    }
  }

  console.log(`Position is ${minpos}`);
  console.log(`Cost is ${mincost}`);
}

main();