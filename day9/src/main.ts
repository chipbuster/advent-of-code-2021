var fs = require("fs");  // file system        

function readArrayFromFile(fn: string): number[][] {
  let contents: string[] = fs.readFileSync(fn, 'utf-8').toString().split('\n');
  let out = contents.map(line => {
    return [...line].map(x => {
      return parseInt(x)
    })
  })
  return out;
}

function stencil4pt(arr: number[][], i: number, j: number): Array<[number, number]> {
  let n = arr.length;
  let m = arr[0].length;
  let neighbors = new Array<[number, number]>();
  if (i - 1 >= 0) {
    neighbors.push([i - 1, j]);
  }
  if (i + 1 < n) {
    neighbors.push([i + 1, j]);
  }
  if (j - 1 >= 0) {
    neighbors.push([i, j - 1]);
  }
  if (j + 1 < m) {
    neighbors.push([i, j + 1]);
  }
  return neighbors;
}

function is_low_point(arr: number[][], i: number, j: number): boolean {
  let neighbors = stencil4pt(arr, i, j);
  let isLower = neighbors.map(x => {
    let i_n = x[0];
    let j_n = x[1];
    return arr[i][j] < arr[i_n][j_n];
  });

  return isLower.every(x => x);
}

function has(arr: [number, number][], target: [number, number]): boolean {
  for (let x of arr) {
    if (x[0] === target[0] && x[1] === target[1]) {
      return true;
    }
  }
  return false;
}

function find_basin_size(arr: number[][], i: number, j: number): number {
  let actives = new Array<[number, number]>();
  let basin = new Array<[number, number]>();

  actives.push([i, j]);
  basin.push([i, j]);
  while (actives.length > 0) {
    let ac = actives[0];
    let ic = ac[0];
    let jc = ac[1];

    if (!has(basin, ac)) {
      basin.push(ac);
    }
    for (let adj of stencil4pt(arr, ic, jc)) {
      let i_n = adj[0];
      let j_n = adj[1];
      if (arr[i_n][j_n] == 9) {
        continue;
      }
      if (has(basin, adj)) {
        continue
      }
      if (has(actives, adj)) {
        continue
      }
      actives.push(adj);
    }
    actives = actives.slice(1, actives.length);
  }

  return basin.length;
}

function main() {
  let input = readArrayFromFile("input.txt");
  let n = input.length;
  let m = input[0].length;

  let basin_sizes = new Array<number>();
  for (let i = 0; i < n; i += 1) {
    for (let j = 0; j < m; j += 1) {
      if (is_low_point(input, i, j)) {
        let basin_size = find_basin_size(input, i, j);
        basin_sizes.push(basin_size);
      }
    }
  }

  basin_sizes.sort((a, b) => { return b - a; })
  console.log(`${basin_sizes}`)
  console.log(basin_sizes.slice(0, 3).reduce((a, b) => a * b, 1));
}

main();
