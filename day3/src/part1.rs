use std::io::BufRead;

const NB_PER_ENTRY: usize = 12;

#[derive(Debug, Eq, PartialEq)]
struct ReportEntry {
    bits: [i8; NB_PER_ENTRY]
}

impl ReportEntry {
    fn new(z: &str) -> Self {
        let mut inp = Vec::new();
        for x in z.chars(){
            inp.push(x.to_digit(10).unwrap().try_into().unwrap());
        }
        assert!(inp.len() == NB_PER_ENTRY, "Got {} chars on input line!", inp.len());
        let x = inp.try_into().unwrap();
        Self {
            bits: x
        }
    }

    fn ith(&self, i: usize) -> i8 {
        self.bits[i]
    }
}

fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let fname = &args[1][..];
    let file = std::fs::File::open(fname).unwrap();
    let freader = std::io::BufReader::new(file).lines();

    let mut report = Vec::new();
    for l in freader {
        let l = l.unwrap();
        report.push(ReportEntry::new(&l))
    }

    let nentries = report.len();
    let mut gamma = vec![0; NB_PER_ENTRY];
    let mut epsilon = vec![0; NB_PER_ENTRY];
    for i in 0..NB_PER_ENTRY {
        let sum = report.iter().map(|x| i32::from(x.ith(i))).sum::<i32>();
        let mean = nentries as f64 / 2.0;
        if sum as f64 > mean {
            gamma [i] = 1;
        } else {
            epsilon[i] = 1;
        }
    }

    // Convert gamma and epsilon into binary
    let mut g = 0;
    for x in gamma {
        g <<= 1;
        match x {
            0 => {},
            1 => g += 1,
            _ => panic!("non 0/1 value in binary string")
        }
    }

    let mut e = 0;
    for x in epsilon {
        e <<= 1;
        match x {
            0 => {},
            1 => e += 1,
            _ => panic!("non 0/1 value in binary string")
        }
    }

    println!("{}, {}, {}", g, e, g * e)


}
