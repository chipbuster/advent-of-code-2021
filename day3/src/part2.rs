use std::io::BufRead;

const NB_PER_ENTRY: usize = 12;

#[derive(Debug, Eq, PartialEq, Clone)]
struct ReportEntry {
    bits: [i8; NB_PER_ENTRY],
}

impl ReportEntry {
    fn new(z: &str) -> Self {
        let mut inp = Vec::new();
        for x in z.chars() {
            inp.push(x.to_digit(10).unwrap().try_into().unwrap());
        }
        assert!(
            inp.len() == NB_PER_ENTRY,
            "Got {} chars on input line!",
            inp.len()
        );
        let x = inp.try_into().unwrap();
        Self { bits: x }
    }

    fn ith(&self, i: usize) -> i8 {
        self.bits[i]
    }

    fn to_dec(&self) -> i32 {
        let mut v = 0;
        for d in self.bits {
            v <<= 1;
            match d {
                0 => {},
                1 => v+=1,
                _ => panic!("Non-bin digit in binary conversion")
            }
        }
        v
    }
}

fn most_common_bit(entries: &[ReportEntry], i: usize) -> i8 {
    let nentries = entries.len();
    let sum = entries
        .iter()
        .map(|x| usize::try_from(x.ith(i)).unwrap())
        .sum::<usize>();
    let halfsize = nentries >> 1;
    match sum.cmp(&halfsize) {
            std::cmp::Ordering::Less => 0,
            std::cmp::Ordering::Greater => 1,
            std::cmp::Ordering::Equal => {
                if nentries & 1 == 1 {
                    0
                } else {
                    1
                }
            }
    }
}

fn least_common_bit(entries: &[ReportEntry], i: usize) -> i8 {
    let nentries = entries.len();
    let sum = entries
        .iter()
        .map(|x| usize::try_from(x.ith(i)).unwrap())
        .sum::<usize>();
            let halfsize = nentries >> 1;
    match sum.cmp(&halfsize) {
            std::cmp::Ordering::Less => 1,
            std::cmp::Ordering::Greater => 0,
            std::cmp::Ordering::Equal => {
                if nentries & 1 == 1 {
                    1
                } else {
                    0
                }
            }
    }
}



fn find_oxy_rating(data: &[ReportEntry]) -> i32 {
    let mut vec = data.clone().to_vec();
    for i in 0..NB_PER_ENTRY {
        if vec.len() == 1 {
            return vec[0].to_dec();
        }

        let mc = most_common_bit(&vec, i);
        println!("mc={}, candidates = {:?}", mc, vec);
        vec = vec.into_iter().filter(|x| x.ith(i) == mc).collect();
    }
    if vec.len() == 1 {
        return vec[0].to_dec()
    }
    panic!("No oxy rating found")
}

fn find_co2_rating(data: &[ReportEntry]) -> i32 {
    let mut vec = data.clone().to_vec();
    for i in 0..NB_PER_ENTRY {
        if vec.len() == 1 {
            return vec[0].to_dec();
        }

        let mc = least_common_bit(&vec, i);
        vec = vec.into_iter().filter(|x| x.ith(i) == mc).collect();
    }
    if vec.len() == 1 {
        return vec[0].to_dec()
    }
    panic!("No co2 rating found")
}


fn main() {
    let args = std::env::args().collect::<Vec<_>>();
    let fname = &args[1][..];
    let file = std::fs::File::open(fname).unwrap();
    let freader = std::io::BufReader::new(file).lines();

    let mut report = Vec::new();
    for l in freader {
        let l = l.unwrap();
        report.push(ReportEntry::new(&l))
    }

    let o2 = find_oxy_rating(&report[..]);
    let co2 = find_co2_rating(&report[..]);

    println!("{}, {}, {}", o2, co2, o2*co2);
}
